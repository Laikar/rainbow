use std::path::PathBuf;
use rainbow::formats::*;
use rainbow::patching_error::PatchingError;
use rainbow::{utils};

pub(crate) fn verify(patch_path: &PathBuf, source: Option<&PathBuf>, target: Option<&PathBuf>, format : PatchFormat) -> Result<String, PatchingError>{
    let patch_file = utils::load_file_content(patch_path)?;

    let patch_output = match format.verify_patch {
        None => {
            format!("{} Format can't verify patch integrity", format.display_name)
        }
        Some(verify_fun) => {
            if verify_fun(&patch_file)? {
                format!("File {} is a valid {} file", patch_path.file_name().unwrap().to_str().unwrap(), format.display_name)
            }else {
                format!("{} Format can't verify patch file integrity", format.display_name)
            }

        }
    };


    let mut source_output : Option<String> = None;
    let mut target_output : Option<String> = None;

    if let Some(source) = source {
        let source_file = utils::load_file_content(source)?;
        source_output = Option::from(match format.verify_source {
            None => {
                format!("{} Format can't verify source file integrity", format.display_name)
            }
            Some(verify_fun) => {
                if verify_fun(&patch_file, &source_file)? {
                    format!("File {} is the valid source for patch {}", source.file_name().unwrap().to_str().unwrap(), patch_path.file_name().unwrap().to_str().unwrap())
                } else {
                    format!("File {} isn't the source for patch {}", source.file_name().unwrap().to_str().unwrap(), patch_path.file_name().unwrap().to_str().unwrap())
                }
            }
        })
    }
    if let Some(target) = target {
        let target_file = utils::load_file_content(target)?;
        target_output = Option::from(match format.verify_target {
            None => { format!("{} Format can't verify target file integrity", format.display_name) }
            Some(verify_fun) => {
                if verify_fun(& patch_file, &target_file)? {
                    format!("File {} is the correct target for patch {}", target.file_name().unwrap().to_str().unwrap(), patch_path.file_name().unwrap().to_str().unwrap())
                } else {
                    format!("File {} isn't the target for patch {}", target.file_name().unwrap().to_str().unwrap(), patch_path.file_name().unwrap().to_str().unwrap())
                }
            }
        })
    }
    return match (source_output.is_some(), target_output.is_some()) {
        (true, true) => { Ok(format!("{}\n{}\n{}", patch_output, source_output.unwrap(), target_output.unwrap())) },
        (false, true) => { Ok(format!("{}\n{}", patch_output, target_output.unwrap())) },
        (true, false) => { Ok(format!("{}\n{}", patch_output, source_output.unwrap())) },
        (false, false) => { Ok(format!("{}", patch_output)) },
    };
}