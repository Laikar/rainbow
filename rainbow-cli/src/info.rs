use std::path::PathBuf;
use rainbow::formats::*;
use rainbow::patching_error::PatchingError;
use rainbow::{utils, UpsPatch};
use itertools::Itertools;


pub (crate) fn info(patch_path: &PathBuf, format : PatchFormat) -> Result<String, PatchingError>{
    let patch_file = utils::load_file_content(patch_path)?;
    return match format.metadata {
        None => {
            Ok(format!("{} Format doesn't contain any info on the patch itself", format.display_name))
        }
        Some(metadata_fn) => {
            let metadata_dict = metadata_fn(&patch_file)?;
            let mut output = format!("File {} is a valid {} patch with the following properties", patch_path.display(), format.display_name);
            for (key, value) in metadata_dict.iter().sorted() {
                output.push_str(format!("\n{}:{}", key, value).as_str())
            }
            Ok(output)
        }
    }
}