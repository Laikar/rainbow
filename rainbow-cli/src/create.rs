use std::path::PathBuf;
use rainbow::formats::*;
use rainbow::patching_error::PatchingError;
use rainbow::{utils,};

pub (crate) fn create(source: &PathBuf, target: &PathBuf, patch_path: &PathBuf, format : PatchFormat, skip_verifications:bool) -> Result<String, PatchingError>{
    let source_file = utils::load_file_content(source)?;
    let target_file = utils::load_file_content(target)?;

    let patch_file_content = match format.create {
        None => {
            return Err(PatchingError::FormatError)
        }
        Some(create_fun) => {
            create_fun(&source_file, &target_file)?
        }
    };

    utils::write_content_to_file(patch_path, &patch_file_content)?;
    Ok(format!("Successfully created patch {} from source {} and target{}", patch_path.display(), source.display(), target.display()))
}