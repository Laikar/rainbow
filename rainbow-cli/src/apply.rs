use std::path::PathBuf;
use rainbow::formats::*;
use rainbow::patching_error::PatchingError;
use rainbow::{utils};

pub (crate) fn apply(source: &PathBuf, target: &PathBuf, patch_path: &PathBuf, format : PatchFormat, skip_verifications:bool) -> Result<String, PatchingError>{
    let source_file_content = utils::load_file_content(source)?;
    let patch_file_content = utils::load_file_content(patch_path)?;

    let final_file_content = match format.apply {
        None => {
            return Err(PatchingError::FormatError)
        }
        Some(apply_fun) => {
            apply_fun(&patch_file_content, &source_file_content)?
        }
    };
    utils::write_content_to_file(target, &final_file_content)?;
    Ok(format!("Successfully patched {} with patch {}, target file is {}", source.display(), patch_path.display(), target.display()))
}