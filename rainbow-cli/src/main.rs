use std::path::PathBuf;

use clap::{Parser, Subcommand};


use rainbow::formats::{PatchFormat, UPS_FORMAT};


mod apply;
mod create;
mod info;
mod verify;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Rainbow{
    #[clap(subcommand)]
    action: Action,

    #[clap(short, long)]
    format: Option<String>
}

#[derive(Subcommand, Debug)]
enum Action{
    Create{
        #[clap(parse(from_os_str))]
        source: PathBuf,
        #[clap(parse(from_os_str))]
        target: PathBuf,
        #[clap(parse(from_os_str))]
        patch: PathBuf,
        #[clap(long)]
        skip_verifications: bool,
    },
    Apply{

        #[clap(parse(from_os_str))]
        source: PathBuf,
        #[clap(parse(from_os_str))]
        patch: PathBuf,
        #[clap(parse(from_os_str))]
        target: PathBuf,
        #[clap(long)]
        skip_verifications: bool,


    },

    Info {
        #[clap(parse(from_os_str))]
        patch: PathBuf,
    },
    Verify{
        #[clap(parse(from_os_str))]
        patch: PathBuf,
        #[clap(short, long, parse(from_os_str))]
        source: Option<PathBuf>,
        #[clap(short, long, parse(from_os_str))]
        target: Option<PathBuf>,
    },
}


fn main() {
    let opt: Rainbow = Rainbow::parse();
    let format: PatchFormat = match opt.format {
        None => { UPS_FORMAT}
        Some(content) => match content.as_str(){
            "ups" => UPS_FORMAT,
            _ => UPS_FORMAT
        }
    };
    match &opt.action {
        Action::Create { source, target, patch, skip_verifications } => {
            match create::create(source, target, patch, format, *skip_verifications) {
                Ok(msg) => {println!("{}", msg)}
                Err(why) => {println!("Error creating: {}", why)}
            }
        }
        Action::Apply { source, target, patch, skip_verifications } => {
            match apply::apply(source, target, patch, format, *skip_verifications) {
                Ok(msg) => {println!("{}", msg)}
                Err(why) => {println!("Error applying: {}", why)}
            }

        }
        Action::Info { patch} => {
            match info::info(patch, format) {
                Ok(msg) => {println!("{}", msg)}
                Err(why) => {println!("Error loading patch: {}", why)}
            }
        }
        Action::Verify { source, patch, target }=> {
            match verify::verify(patch, Option::from(source), Option::from(target), format) {
                Ok(msg) => {println!("{}", msg)}
                Err(why) => {println!("Error verifying patch: {}", why)}
            }
        }
    }
}
