use std::ffi::OsStr;
use std::fs::{File, create_dir};
use std::{env, fs};

use std::io::{Error, Read};

use std::process::{Command, Output};

use std::str;

use assert_cmd::prelude::CommandCargoExt;
use std::path::PathBuf;
use fs_extra::dir::CopyOptions;

pub const SAMPLE_PATH: &str = "tests/samples";

pub const SOURCE_SAMPLE: &str = "samples/source.txt";
pub const TARGET_SAMPLE_1: &str = "samples/final1.txt";
pub const PATCH_SAMPLE_1: &str = "samples/patch1.ups";
pub const TARGET_SAMPLE_2: &str = "samples/final2.txt";
pub const PATCH_SAMPLE_2: &str = "samples/patch2.ups";

pub const TARGET_OUTPUT_1: &str = "output/final1.txt";
pub const PATCH_OUTPUT_1: &str = "output/patch1.ups";
pub const TARGET_OUTPUT_2: &str = "output/final2.txt";
pub const PATCH_OUTPUT_2: &str = "output/patch2.ups";


pub fn run_cli_with_args<I, S>(args: I) -> Output
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
{
    let output = match Command::cargo_bin("rainbow-cli").unwrap()
        .args(args)
        .output(){
        Ok(out) => out,
        Err(_) => {panic!("Failed to run file")}
    };
    return output
}

pub fn load_file_content(path: &str) -> Vec<u8> {
    let mut patch_file = File::open(path).unwrap();
    let mut content : Vec<u8> = vec![];
    patch_file.read_to_end(&mut content);
    return content
}

pub fn setup(){
    //TMP dir from OS
    let tmp_dir = env::temp_dir();
    let project_folder = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    env::set_current_dir(project_folder);
    //Tests dir
    let tests_dir = tmp_dir.join("rainbow_tests");
    //We delete the tests dir if it exists
    if (&tests_dir).exists()  {
        if tests_dir.metadata().unwrap().is_dir() {
            fs::remove_dir_all(&tests_dir).unwrap();
        }else {
            fs::remove_file(&tests_dir).unwrap();
        }

    }
    
    create_dir(&tests_dir).unwrap();
    let output_dir = tests_dir.join("output");
    create_dir(output_dir).unwrap();
    let sample_dir = PathBuf::from(SAMPLE_PATH);

    let options = CopyOptions::new();
    println!("{}", env::current_dir().unwrap().display());
    fs_extra::dir::copy(sample_dir, &tests_dir, &options).unwrap();
    env::set_current_dir(&tests_dir).unwrap()
}

#[cfg(test)]
mod general {

    use super::*;

    #[test]
    fn can_setup_tests(){
        setup();
        
    }


    #[test]
    fn can_show_help(){
        let output = run_cli_with_args(&["help"]);
        println!("stderr:\n{}", str::from_utf8(&*output.stderr).unwrap());
        println!("stdout:\n{}", str::from_utf8(&*output.stdout).unwrap());

    }
    #[test]
    fn apply_help(){
        let output = run_cli_with_args(&["help", "apply"]);
        println!("stderr:\n{}", str::from_utf8(&*output.stderr).unwrap());
        println!("stdout:\n{}", str::from_utf8(&*output.stdout).unwrap());

    }


}
#[cfg(test)]
mod ups{
    use std::{str, fs};
    use super::*;


    #[test]
    fn can_apply(){
        setup();
        let cmd_output1 = run_cli_with_args(&["apply", SOURCE_SAMPLE, PATCH_SAMPLE_1, TARGET_OUTPUT_1]);
        println!("stderr:\n{}", str::from_utf8(&*cmd_output1.stderr).unwrap());
        println!("stdout:\n{}", str::from_utf8(&*cmd_output1.stdout).unwrap());
        let sample1 = load_file_content(TARGET_SAMPLE_1);
        let output1 = load_file_content(TARGET_OUTPUT_1);
        assert_eq!(sample1, output1);

        let cmd_output2 = run_cli_with_args(&["apply", SOURCE_SAMPLE, PATCH_SAMPLE_2, TARGET_OUTPUT_2]);
        println!("stderr:\n{}", str::from_utf8(&*cmd_output2.stderr).unwrap());
        println!("stdout:\n{}", str::from_utf8(&*cmd_output2.stdout).unwrap());
        let sample2 = load_file_content(TARGET_SAMPLE_2);
        let output2 = load_file_content(TARGET_OUTPUT_2);
        assert_eq!(sample2, output2);
        

    }
    #[test]
    fn can_create(){
        setup();
        let cmd_output1 = run_cli_with_args(&["create", SOURCE_SAMPLE, TARGET_SAMPLE_1, PATCH_OUTPUT_1]);
        println!("stderr:\n{}", str::from_utf8(&*cmd_output1.stderr).unwrap());
        println!("stdout:\n{}", str::from_utf8(&*cmd_output1.stdout).unwrap());
        let sample1 = load_file_content(PATCH_SAMPLE_1);
        let output1 = load_file_content(PATCH_OUTPUT_1);
        assert_eq!(sample1, output1);

        let cmd_output2 = run_cli_with_args(&["create", SOURCE_SAMPLE, TARGET_SAMPLE_2, PATCH_OUTPUT_2]);
        println!("stderr:\n{}", str::from_utf8(&*cmd_output2.stderr).unwrap());
        println!("stdout:\n{}", str::from_utf8(&*cmd_output2.stdout).unwrap());
        let sample2 = load_file_content(PATCH_SAMPLE_2);
        let output2 = load_file_content(PATCH_OUTPUT_2);
        assert_eq!(sample2, output2);
        
    }
    #[test]
    fn can_show_info(){
        setup();
        let cmd_output = run_cli_with_args(&["info", PATCH_SAMPLE_1]);
        println!("stderr:\n{}", str::from_utf8(&*cmd_output.stderr).unwrap());
        println!("stdout:\n{}", str::from_utf8(&*cmd_output.stdout).unwrap());
        assert_eq!(str::from_utf8(&*cmd_output.stdout).unwrap(), "File samples/patch1.ups is a valid UPS patch with the following properties
Source file crc32 checksum:0x29E0B36E
Source file size:28 B
Target file crc32 checksum:0x23A777E3
Target file size:27 B
");
        
    }

    #[test]
    fn can_verify(){
        setup();
        let cmd_output1 = run_cli_with_args(&["verify", PATCH_SAMPLE_1, "-s", SOURCE_SAMPLE]);
        println!("stderr1:\n{}", str::from_utf8(&*cmd_output1.stderr).unwrap());
        println!("stdout1:\n{}", str::from_utf8(&*cmd_output1.stdout).unwrap());
        assert_eq!(str::from_utf8(&*cmd_output1.stdout).unwrap(), "File patch1.ups is a valid UPS file
File source.txt is the valid source for patch patch1.ups
");
        let cmd_output2 = run_cli_with_args(&["verify", PATCH_SAMPLE_2, "-s", SOURCE_SAMPLE]);
        println!("stderr2:\n{}", str::from_utf8(&*cmd_output2.stderr).unwrap());
        println!("stdout2:\n{}", str::from_utf8(&*cmd_output2.stdout).unwrap());
        assert_eq!(str::from_utf8(&*cmd_output2.stdout).unwrap(), "File patch2.ups is a valid UPS file
File source.txt is the valid source for patch patch2.ups
");
        let cmd_output3 = run_cli_with_args(&["verify", PATCH_SAMPLE_1, "-t", TARGET_SAMPLE_1]);
        println!("stderr3:\n{}", str::from_utf8(&*cmd_output3.stderr).unwrap());
        println!("stdout3:\n{}", str::from_utf8(&*cmd_output3.stdout).unwrap());
        assert_eq!(str::from_utf8(&*cmd_output3.stdout).unwrap(), "File patch1.ups is a valid UPS file
File final1.txt is the correct target for patch patch1.ups
");
        let cmd_output4 = run_cli_with_args(&["verify", PATCH_SAMPLE_2, "-t", TARGET_SAMPLE_2]);
        println!("stderr4:\n{}", str::from_utf8(&*cmd_output4.stderr).unwrap());
        println!("stdout4:\n{}", str::from_utf8(&*cmd_output4.stdout).unwrap());
        assert_eq!(str::from_utf8(&*cmd_output4.stdout).unwrap(), "File patch2.ups is a valid UPS file
File final2.txt is the correct target for patch patch2.ups
");
        
    }
}

