use std::io;

use ups::UpsError;
use std::fmt::{Display, Formatter};
use std::path::PathBuf;


#[derive(Debug,)]
pub enum PatchingError{
    IoError (io::Error , PathBuf),
    UpsError(UpsError),
    FormatError,


}


impl From<ups::UpsError> for PatchingError{
    fn from(e: UpsError) -> Self {
        PatchingError::UpsError(e)
    }
}

impl Display for PatchingError{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        return match self {
            PatchingError::IoError(e, p) => {
                write!(f, "{}, {}", e, p.to_str().unwrap())
            }
            PatchingError::UpsError(e) => {
                write!(f, "{}", e)
            }
            _ => {
                write!(f, "{}", "Unknown error")
            }
        }
    }
}