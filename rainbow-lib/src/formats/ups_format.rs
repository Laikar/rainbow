use ups;
use crate::formats::PatchFormat;
use crate::patching_error::PatchingError;
use std::collections::HashMap;
use bytesize::ByteSize;

fn ups_apply(patch: &Vec<u8>, source: &Vec<u8>) -> Result<Vec<u8>, PatchingError>{
    let patch = ups::UpsPatch::load(&patch)?;
    return Ok(patch.apply(&source)?)

}

fn ups_create(source: &Vec<u8>, target: &Vec<u8>) -> Result<Vec<u8>, PatchingError>{
    let patch = ups::UpsPatch::create(&source, &target);
    return Ok(patch.get_patch_file_contents())

}
fn ups_is_patch(patch: &Vec<u8>) -> bool{
    return  patch[0..4] == ups::UpsPatch::CANON_HEADER
}

fn ups_verify_patch(patch: &Vec<u8>) -> Result<bool, PatchingError>{
    let result = ups::UpsPatch::load(patch);
    return Ok(result.is_ok())
}

fn ups_verify_source(patch: &Vec<u8>, source: &Vec<u8>) -> Result<bool, PatchingError>{
    let patch = ups::UpsPatch::load(&patch)?;
    return Ok(patch.file_is_source(&source))

}

fn ups_verify_target(patch: &Vec<u8>, target: &Vec<u8>) -> Result<bool, PatchingError>{
    let patch = ups::UpsPatch::load(&patch)?;
    return Ok(patch.file_is_target(&target))

}
fn ups_info(patch: &Vec<u8>)-> Result<HashMap<String, String>, PatchingError>{
    let patch = ups::UpsPatch::load(&patch)?;
    let mut output: HashMap<String, String> = HashMap::new();
    output.insert(String::from("Source file size"), ByteSize::b(patch.source_file_size).to_string());
    output.insert(String::from("Source file crc32 checksum"), format!("{:#X}", patch.source_crc32));
    output.insert(String::from("Target file size"), ByteSize::b(patch.target_file_size).to_string());
    output.insert(String::from("Target file crc32 checksum"), format!("{:#X}", patch.target_crc32));
    return Ok(output);


}

pub const UPS_FORMAT: PatchFormat = PatchFormat {
    display_name: "UPS",
    apply: Some(ups_apply),
    create: Some(ups_create),
    is_patch: Some(ups_is_patch),
    verify_patch: Some(ups_verify_patch),
    verify_source: Some(ups_verify_source),
    verify_target: Some(ups_verify_target),
    metadata: Some(ups_info)
};
