mod ups_format;

use crate::patching_error::PatchingError;
pub use ups_format::UPS_FORMAT;
use std::collections::HashMap;

type ApplyFn = fn (&Vec<u8>, &Vec<u8>)-> Result<Vec<u8>, PatchingError>;
type CreateFn = fn (&Vec<u8>, &Vec<u8>)-> Result<Vec<u8>, PatchingError>;
type VerifyPatchFn = fn (&Vec<u8>) -> Result<bool, PatchingError>;
type VerifySourceFn = fn (&Vec<u8>, &Vec<u8>)-> Result<bool, PatchingError>;
type VerifyTargetFn = fn (&Vec<u8>, &Vec<u8>)-> Result<bool, PatchingError>;
type MetadataFn = fn (&Vec<u8>) -> Result<HashMap<String, String>, PatchingError>;
type IsPatchFn = fn (&Vec<u8>) -> bool;

pub struct PatchFormat {

    pub display_name: &'static str,
    pub apply: Option<ApplyFn>,
    pub create: Option<CreateFn>,
    pub is_patch: Option<IsPatchFn>,
    pub verify_patch: Option<VerifyPatchFn>,
    pub verify_source: Option<VerifySourceFn>,
    pub verify_target: Option<VerifyTargetFn>,
    pub metadata: Option<MetadataFn>,

}

impl PatchFormat{
    pub fn guess(patch: &Vec<u8>)-> Option<PatchFormat>{
        if UPS_FORMAT.is_patch.unwrap()(patch){
            return Some(UPS_FORMAT);
        }
        return None
    }
}
