use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;


use crate::patching_error::PatchingError;

pub fn write_content_to_file(path: &PathBuf, content: &Vec<u8>) -> Result<(), PatchingError> {
    let mut file = match File::create(path){
        Ok(f) => f,
        Err(e) => return Err(PatchingError::IoError(e, PathBuf::from(path))),
    };
    match file.write_all(content){
        Ok(_) => {Ok(()) }
        Err(e) => {Err(PatchingError::IoError(e, path.clone()))}
    }
}
pub fn load_file_content(path: &PathBuf) ->  Result<Vec<u8>, PatchingError> {
    let mut file = match File::open(path){
        Ok(f) => f,
        Err(e) => return Err(PatchingError::IoError(e, PathBuf::from(path))),
    };
    let mut content : Vec<u8> = vec![];
    match file.read_to_end(&mut content) {
        Ok(_) => {Ok(content)}
        Err(e) => {Err(PatchingError::IoError(e, path.clone()))}
    }

}